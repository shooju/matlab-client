%% Connect
Shooju.Connect(SERVER, USER, API_KEY);
Shooju.EnableDebugLog(true);

%% Register
Shooju.RegisterJob('matlab example_write',2);
disp(strcat({'JobId: '}, num2str(Shooju.JobId)));

%% Write
Shooju.Write('sid="users\\alexey\\matlab\\test1"', 'fld2', '"val2"');
Shooju.Write('sid="users\\alexey\\matlab\\test1"', 'fld1', '"val1"');


%% Write
Shooju.Write('sid="users\\alexey\\matlab\\test2"', {}, {}, '2020-04-01', 2.2);
Shooju.Write('sid="users\\alexey\\matlab\\test2"', {}, {}, '2020-04-02', 1.1);

%% Write
% fld2 = null
Shooju.Write('sid="users\\alexey\\matlab\\test1"', 'fld2', nan, {'2020-04-01'; '2020-04-01'}, {4.4; 5.5}, 'points');

%% Write
Shooju.Write('sid="users\\alexey\\matlab\\test2"', {'fld3_num'; 'fld4_num'}, {42; 36.6}, {}, {}, 'all');


%% Submit
Shooju.EnableDebugDump(true);
Shooju.SubmitJob();
Shooju.EnableDebugDump(false);

%% Finish
Shooju.FinishJob