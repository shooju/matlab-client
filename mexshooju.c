
#define VERSION "3.2.0"

#define FLAG_SJTS 1
#define FLAG_LOGS 2
#define FLAG_DUMP 4

#include "mex.h"
#include "build/c-client/shooju.h"
#include "build/sjts/sjts.h"
#include <time.h>
#include <inttypes.h>
#include <windows.h>
#include <string.h>


FILE *g_log;

mxArray *decodeSJTS(const char *json_str, size_t json_size, int output_type);
int copy_escaped(char *buf, const char *str);

JSINT64
getDate(JSOBJ obj, int idx)
{
	JSINT64 r = 0;
	mxArray *v = mxGetCell((mxArray*)obj, idx);
	if (v) {
		if (mxIsChar(v)) {
			const char *s = mxArrayToString(v);
			r = iso_to_milli(s);
			mxFree(s);
		} else {
			r = (JSINT64)mxGetScalar(v);
		}
	}
	return r;
}

double getValue(JSOBJ obj, int idx)
{
	mxArray *v = mxGetCell((mxArray*)obj, idx);
	if (v && mxIsNumeric(v)) return mxGetScalar(v);
	return 0;
}

static char *
check_buf(SJTSObjectEncoder *enc, char *p, int sz)
{
	char *old = enc->outBuffer;
	size_t offs = p - old;
	size_t oldSize = enc->outSize;
	while (offs + sz >= enc->outSize) enc->outSize += enc->outSize / 4;
	if (oldSize == enc->outSize) return p;
	enc->outBuffer = mxRealloc(old, enc->outSize);
	enc->items = (SJItem*)enc->outBuffer;
	for (int i = 0; i < enc->nItems; i++) {
		enc->items[i].buffer = enc->outBuffer + (enc->items[i].buffer - old);
	}
	return enc->outBuffer + offs;
}

static char *
stpcpy(char *dst, char *src)
{
	size_t sz = strlen(src);
	memcpy(dst, src, sz);
	return dst + sz;
}

int
sjtsFunction(struct shooju *shj, const mxArray *params, int output_flags, int (*func)(struct shooju *, const mxArray *, SJTSObjectEncoder *))
{
	SJTSObjectEncoder enc;
	enc.outBuffer = NULL;
	int res = func(shj, params, &enc);
	if (res >= 0 && (output_flags & FLAG_DUMP)) {
		FILE *f = fopen("mexshooju_req.dat", "wb");
		fwrite(enc.outBuffer, 1, enc.outSize, f);
		fclose(f);
	}
	mxFree(enc.outBuffer);
	return res;
}


char*
copyElement(char *p, mxArray *ptr, SJTSObjectEncoder *enc)
{
	if (!ptr) return NULL;
	if (mxIsChar(ptr)) {
		char *val = mxArrayToString(ptr);
		p = check_buf(enc, p, copy_escaped(NULL, val) + 100);
		p += copy_escaped(p, val);
		mxFree(val);
	} else if (mxIsNumeric(ptr)) {
		double db = mxGetScalar(ptr);
		if (db != db) {
			p = stpcpy(p, "null");
		} else {
			p += sprintf(p, "%lg", db);
		}
	} else if (mxIsCell(ptr)) {
		int count = (int)mxGetNumberOfElements(ptr);
		*p++ = '[';
		for (int i = 0; i < count; i++) {
			p = copyElement(p, mxGetCell(ptr, i), enc);
			*p++ = ',';
		}
		if (count) p--;
		*p++ = ']';
	} else {
		return NULL;
	}
	return p;
}

int
shoojuWrite(struct shooju *shj, const mxArray *params, SJTSObjectEncoder *enc)
{
	mxArray *ptr = mxGetCell(params, 2), *st, *ptr1, *ptr2;
	if (!mxIsNumeric(ptr)) {
		return -12;
	}
	enc->getDate = getDate;
	enc->getValue = getValue;
	enc->ujson.malloc = mxMalloc;
	enc->ujson.realloc = mxRealloc;
	int job_id = mxGetScalar(ptr);
	if (g_log) fprintf(g_log, "Write job_id: %d\n", job_id);
	ptr = mxGetCell(params, 3);
	if (!mxIsStruct(ptr)) {
		return -13;
	}
	st = ptr;
	enc->nItems = (int)mxGetNumberOfElements(ptr);
	if (g_log) fprintf(g_log, "n_series: %d\n", (int)enc->nItems);
	enc->outSize = (sizeof(SJItem) + 100) * enc->nItems;
	if (enc->outSize > 1000000) enc->outSize = 1000000;
	enc->outBuffer = mxMalloc(enc->outSize);
	enc->items = (SJItem*)enc->outBuffer;
	char *p = (char*)(enc->items + enc->nItems);
	for (int i = 0; i < enc->nItems; i++) {
		enc->items[i].buffer = p;
		char *val;
		size_t sz;
		int n_fields = 0, n_points = 0;
		ptr = mxGetFieldByNumber(st, i, 0);
		if (!ptr || !mxIsChar(ptr)) {
			return -14;
		}
		val = mxArrayToString(ptr);
		p = check_buf(enc, p, copy_escaped(NULL, val) + 100);
		p = stpcpy(p, "{\"series_query\":");
		p += copy_escaped(p, val);
		mxFree(val);
		ptr = mxGetFieldByNumber(st, i, 5);
		if (!ptr || !mxIsChar(ptr)) {
			return -15;
		}
		val = mxArrayToString(ptr);
		if (strcmp(val, "none")) {
			p = check_buf(enc, p, copy_escaped(NULL, val) + 100);
			p = stpcpy(p, ",\"keep_only\":");
			p += copy_escaped(p, val);
		}
		mxFree(val);
		ptr = mxGetFieldByNumber(st, i, 3);
		if (ptr) {
			n_points =  (int)mxGetNumberOfElements(ptr);
			enc->items[i].obj_stamp = ptr;
			ptr2 = mxGetFieldByNumber(st, i, 4);
			if (!ptr2 || n_points != (int)mxGetNumberOfElements(ptr2)) {
				return -16;
			}
			enc->items[i].obj_value = ptr2;
		}
		ptr = mxGetFieldByNumber(st, i, 1);
		if (ptr) {
			ptr1 = ptr;
			n_fields =  (int)mxGetNumberOfElements(ptr);
			ptr2 = mxGetFieldByNumber(st, i, 2);
			if (!ptr2 || n_fields != (int)mxGetNumberOfElements(ptr2)) {
				return -17;
			}
		}
		for (int j = 0; j < n_fields; j++) {
			ptr = mxGetCell(ptr1, j);
			if (!ptr || !mxIsChar(ptr)) {
				return -18;
			}
			val = mxArrayToString(ptr);
			p = check_buf(enc, p, copy_escaped(NULL, val) + 100);
			p = stpcpy(p, j ? "," : ",\"fields\":{");
			p += copy_escaped(p, val);
			mxFree(val);
			*p++ = ':';
			p = copyElement(p, mxGetCell(ptr2, j), enc);
			if (!p) {
				return -19;
			}
		}
		p += sprintf(p, "},\"points\":%d}" + (n_fields ? 0 : 1), n_points);
		enc->items[i].sz = p - enc->items[i].buffer;
		enc->items[i].len = n_points;
	}
	p = check_buf(enc, p, 100);
	enc->rootBuffer = p;
	sprintf(p, "{\"series\":%d}", (int)enc->nItems);
	p = enc->outBuffer;
	ProcessItems(enc);
	shooju_set_send_format(shj, "sjts");
	int res = shooju_series_write(shj, job_id, enc->outBuffer, enc->outSize);
	mxFree(p);
	return res;
}

int
shoojuRawPost(struct shooju *shj, const mxArray *params, SJTSObjectEncoder *enc)
{
	mxArray *ptr = mxGetCell(params, 2), *st, *ptr1, *ptr2;
	if (!mxIsChar(ptr)) {
		return -12;
	}
	enc->getDate = getDate;
	enc->getValue = getValue;
	enc->ujson.malloc = mxMalloc;
	enc->ujson.realloc = mxRealloc;
	char *url = mxArrayToString(ptr);
	if (g_log) fprintf(g_log, "rawPost url: %s\n", url);
	ptr = mxGetCell(params, 3);
	if (!mxIsStruct(ptr)) {
		return -13;
	}
	st = ptr;
	for (int i = 0; i < mxGetNumberOfFields(st); i++) {
		char *name = mxGetFieldNameByNumber(st, i);
		ptr = mxGetField(st, 0, name);
		char *value = mxArrayToString(ptr);
		shooju_add_param(shj, name, value);
		mxFree(value);
	}
	ptr = mxGetCell(params, 4);
	if (!mxIsStruct(ptr)) {
		return -14;
	}
	st = ptr;
	enc->nItems = (int)mxGetNumberOfElements(ptr);
	if (g_log) fprintf(g_log, "n_series: %d\n", (int)enc->nItems);
	enc->outSize = (sizeof(SJItem) + 100) * enc->nItems;
	if (enc->outSize > 1000000) enc->outSize = 1000000;
	enc->outBuffer = mxMalloc(enc->outSize);
	enc->items = (SJItem*)enc->outBuffer;
	char *p = (char*)(enc->items + enc->nItems);
	for (int i = 0; i < enc->nItems; i++) {
		enc->items[i].buffer = p;
		char *val;
		size_t sz;
		int n_points = 0;
		p = check_buf(enc, p, copy_escaped(NULL, val) + 100);
		p = stpcpy(p, "{\"series_id\":");
		p += copy_escaped(p, NULL);
		ptr = mxGetFieldByNumber(st, i, 0);
		if (ptr) {
			n_points =  (int)mxGetM(ptr);
			enc->items[i].obj_stamp = ptr;
			ptr2 = mxGetFieldByNumber(st, i, 1);
			if (!ptr2 || n_points != (int)mxGetNumberOfElements(ptr2)) {
				return -15;
			}
			enc->items[i].obj_value = ptr2;
		}
		p += sprintf(p, ",\"points\":%d}", n_points);
		enc->items[i].sz = p - enc->items[i].buffer;
		enc->items[i].len = n_points;
	}
	p = check_buf(enc, p, 100);
	enc->rootBuffer = p;
	sprintf(p, "{\"series\":%d}", (int)enc->nItems);
	p = enc->outBuffer;
	ProcessItems(enc);
	shooju_set_send_format(shj, "sjts");
	int res = shooju_series_raw_post(shj, url, enc->outBuffer, enc->outSize);
	mxFree(p);
	return res;
}

int
shoojuFunction(struct shooju *shj, const mxArray *params, int output_flags)
{
	size_t nParams = mxGetNumberOfElements(params);
	mwIndex i;

	for (i = 0; i < nParams / 2; i++) {
		char *name, *value;
		mxArray *name_ptr = mxGetCell(params, i * 2);
		mxArray *value_ptr = mxGetCell(params, i * 2 + 1);
		if (!mxIsChar(name_ptr) || !mxIsChar(value_ptr)) {
			mexErrMsgIdAndTxt("MATLAB:mexshooju:inputNotString",
				"Params must be of type string.\n");
			return 1;
		}
		name = mxArrayToString(name_ptr);
		value = mxArrayToString(value_ptr);
		if ((strcmp(name, "df") == 0 || strcmp(name, "dt") == 0) && strlen(value) > 3) {
			int64_t milli = iso_to_milli(value);
			if (milli == -1) {
				shooju_add_param(shj, name, value);
				if (g_log) fprintf(g_log, "Pass date param '%s' as-is: %s\n", name, value);
			} else {
				char buf[64];
				sprintf(buf, "%" PRId64, milli);
				if (g_log) fprintf(g_log, "Convert date %s = %s to %s milli (timezone: %d)\n",
					name, value, buf, (int)_timezone);
				shooju_add_param(shj, name, buf);
			}
		} else if (strcmp(name, "method") == 0 && !i) {
			mxFree(name);
			if (g_log) fprintf(g_log, "Found method: %s\n", value);
			if (strcmp(value, "register_job") == 0 && nParams == 3) {
				mxArray *par_ptr = mxGetCell(params, 2);
				if (mxIsChar(par_ptr)) {
					char *par = mxArrayToString(par_ptr);
					int res = shooju_register_job(shj, par);
					mxFree(value);
					mxFree(par);
					return res;
				}
			} else if (strcmp(value, "finish_job") == 0 && nParams == 3) {
				mxArray *par_ptr = mxGetCell(params, 2);
				if (mxIsNumeric(par_ptr)) {
					int par = mxGetScalar(par_ptr);
					mxFree(value);
					return shooju_finish_job(shj, par);
				}
			} else if (strcmp(value, "write") == 0 && nParams > 3) {
				mxFree(value);
				return sjtsFunction(shj, params, output_flags, shoojuWrite);
			} else if (strcmp(value, "raw_post") == 0 && nParams == 5) {
				mxFree(value);
				return sjtsFunction(shj, params, output_flags, shoojuRawPost);
			}
			mxFree(value);
			return -10;
		} else {
			shooju_add_param(shj, name, value);
			if (g_log) fprintf(g_log, "Added param %s = %s\n", name, value);
		}
		mxFree(name);
		mxFree(value);
	}

	shooju_set_receive_format(shj, "sjts");
	shooju_set_date_format(shj, sdf_milli);
	return shooju_series(shj);
}


static void
matlabNetworkError(struct shooju *shj, const char *msg)
{
	time_t my_time = time(NULL);
	mexErrMsgIdAndTxt("MATLAB:mexshooju:networkError",
		"%s\n%s %s\nCurrent time: %s\n", msg, shooju_is_post_request(shj) ? "POST" : "GET",
		shooju_get_request_url(shj), ctime(&my_time));
	shooju_done(shj);
	if (g_log) {
		fprintf(g_log, "%s\n", msg);
		fclose(g_log);
	}
}


void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	char *server, *user, *password;
	struct shooju *shj;
	int has_auth;

	if (nrhs == 0) {
		plhs[0] = mxCreateString(VERSION);
		return;
	}

	/* Check number of input params */
	if (nrhs < 3 || nrhs > 5) {
		mexErrMsgIdAndTxt("MATLAB:mexshooju:nrhs",
			"Three, four or five inputs required.\n");
		return;
	}

	/* Check to be sure input is of type char */
	if (!(mxIsChar(prhs[0]) && mxIsChar(prhs[1]) && mxIsChar(prhs[2]))) {
		mexErrMsgIdAndTxt("MATLAB:mexshooju:inputNotString",
			"First three inputs must be of type string.\n");
		return;
	}

	if (nrhs == 4 && !mxIsCell(prhs[3])) {
		mexErrMsgIdAndTxt("MATLAB:mexshooju:inputNotCell",
			"Fourth input must be of type cell.\n");
		return;
	}

	if (nrhs == 5 && !mxIsNumeric(prhs[4])) {
		mexErrMsgIdAndTxt("MATLAB:mexshooju:inputNotNumeric",
			"Fifth input must be of type numeric.\n");
		return;
	}

	server = mxArrayToString(prhs[0]);
	user = mxArrayToString(prhs[1]);
	password = mxArrayToString(prhs[2]);
	has_auth = *user || *password;
	shj = shooju_init(server, password, user);
	mxFree(password);
	mxFree(user);
	mxFree(server);
	shooju_set_location_type(shj, "matlab");

	int output_flags = 0;
	if (nrhs == 5) {
		output_flags = (int)mxGetScalar(prhs[4]);
	}

	if (output_flags & FLAG_LOGS) {
		g_log = fopen("mexshooju.log", "w");
	} else {
		g_log = NULL;
	}

	DWORD start = GetTickCount();

	shooju_set_send_format(shj, NULL);
	shooju_set_receive_format(shj, NULL);

	int res = (nrhs > 3 && has_auth) ? shoojuFunction(shj, prhs[3], output_flags) : shooju_ping(shj);
	if (res) {
		char buf[256];
		sprintf(buf, "Network request error: %d", res);
		matlabNetworkError(shj, buf);
		return;
	}

	if (output_flags & FLAG_DUMP) {
		FILE *f = fopen("mexshooju_res.dat", "w");
		fwrite(shooju_get_body(shj), shooju_get_body_len(shj), 1, f);
		fclose(f);
	}

	if (g_log) {
		mexPrintf("url = %s\n", shooju_get_request_url(shj));
		mexPrintf("network time: = %lg\n", (double)(GetTickCount() - start) / 1000);
		start = GetTickCount();
	}

	if (shooju_get_body_len(shj) == 0) {
		matlabNetworkError(shj, "Network error: empty response body");
		return;
	}

	plhs[0] = decodeSJTS(shooju_get_body(shj), shooju_get_body_len(shj), output_flags & FLAG_SJTS);
	int res_code = shooju_get_res_code(shj);
	if (res_code >= 500) {
		mxArray *descr;
		char buf[512];
		if (plhs[0] && mxIsStruct(plhs[0]) && (descr = mxGetField(plhs[0], 0, "description")) && mxIsChar(descr)) {
			const char *s = mxArrayToString(descr);
			sprintf(buf, "Server responded with status %d (%s)", res_code, s);
			mxFree(s);
		} else {
			sprintf(buf, "Server responded with status %d", res_code);
		}
		matlabNetworkError(shj, buf);
		return;
	}

	if (g_log) {
		mexPrintf("decode time: = %lg\n", (double)(GetTickCount() - start) / 1000);
	}

	shooju_done(shj);
	if (g_log) fclose(g_log);
}

typedef struct __SJTSContext {
	int output_type;
	mxArray *arrValues;
	mxArray *arrDates;
	mxArray *arrFields;
	int idx;
	int level;
	size_t n_series;
	size_t n_points;
	size_t n_fields;
} SJTSContext;

static void Object_objectAddKey(void *prv, JSOBJ obj, JSOBJ name, JSOBJ value)
{
	char *n = mxArrayToString(name);
	int field_num;
	mxAddField(obj, n);
	field_num = mxGetFieldNumber(obj, n);
	mxSetFieldByNumber(obj, 0, field_num, value);
	mxFree(n);
	mxDestroyArray(name);
}

static void Object_arrayAddItem(void *prv, JSOBJ obj, JSOBJ value)
{
	int sz = (int)mxGetNumberOfElements(obj);
	if (sz) {
		int limit = 10;
		while (sz > limit) limit = limit * 10;
		if (sz == limit) {
			double *p = mxGetPr(obj);
			mxSetPr(obj, mxRealloc(p, sizeof(mxArray*) * sz * 10));
		}
	} else {
		mxSetPr(obj, mxMalloc(sizeof(mxArray*) * 10));
	}
	mxSetN(obj, sz + 1);
	mxSetCell(obj, sz, value);
}

static JSOBJ Object_newString(void *prv, wchar_t *start, wchar_t *end)
{
	char *str = mxMalloc(end - start + 1);
	int i;
	for (i = 0; i < (end - start); i++) str[i] = (char)start[i];
	str[end - start] = 0;
	mxArray *ret = mxCreateString(str);
	mxFree(str);
	return ret;
}

static JSOBJ Object_newTrue(void *prv)
{
	return mxCreateLogicalScalar(1);
}

static JSOBJ Object_newFalse(void *prv)
{
	return mxCreateLogicalScalar(0);
}

static JSOBJ Object_newNull(void *prv)
{
	return NULL;
}

static JSOBJ Object_newObject(void *prv)
{
	return mxCreateStructMatrix(1, 1, 0, NULL);
}

static JSOBJ Object_newArray(void *prv)
{
	return mxCreateCellMatrix(1, 0);

}

static JSOBJ Object_newInteger(void *prv, JSINT32 value)
{
	return mxCreateDoubleScalar(value);
}

static JSOBJ Object_newLong(void *prv, JSINT64 value)
{
	return mxCreateDoubleScalar(value);
}

static JSOBJ Object_newUnsignedLong(void *prv, JSUINT64 value)
{
	return mxCreateDoubleScalar(value);
}

static JSOBJ Object_newDouble(void *prv, double value)
{
	return mxCreateDoubleScalar(value);
}

static void Object_releaseObject(void *prv, JSOBJ obj)
{
	mxDestroyArray(obj);
}

static int getCount(SJTSObjectDecoder *dec, int level)
{
	const char *key = (level ? "points" : "series");
	SJTSContext *ctx = (SJTSContext *)dec->prv;
	ctx->level = level;
	mxArray *obj = dec->obj[0];
	if (level == 0) {
		mxArray *series = mxGetField(dec->obj[0], 0, key);
		ctx->idx = 0;
		ctx->n_series = 0;
		if (series && mxIsDouble(series)) {
			ctx->n_series = (size_t)mxGetScalar(series);
			dec->arr[level] = mxCreateCellMatrix(1, ctx->n_series);
			mxDestroyArray(series);
			mxSetField(obj, 0, key, dec->arr[level]);
			return ctx->n_series;
		}
	} else {
		mxArray *points = mxGetField(dec->obj[level], 0, key);
		size_t len = 0;
		if (points && mxIsDouble(points)) {
			len = (size_t)mxGetScalar(points);
		}
		if (ctx->idx == 0) ctx->n_points = len;
		if (ctx->output_type) {
			if (ctx->idx == 0) {
				mxArray *fields = mxGetField(dec->obj[level], 0, "fields");
			    ctx->n_fields = fields ? mxGetNumberOfFields(fields) : 0;
			    ctx->arrValues = mxCreateCellMatrix(ctx->n_fields, ctx->n_series);
				mxAddField(obj, "series_values");
				mxSetField(obj, 0, "series_values", ctx->arrValues);
				dec->arr[level] = mxCreateDoubleMatrix(ctx->n_points, ctx->n_series, mxREAL);
				mxAddField(obj, "series_points");
				mxSetField(obj, 0, "series_points", dec->arr[level]);
				ctx->arrDates = mxCreateCellMatrix(ctx->n_points, 1);
				mxAddField(obj, "series_dates");
				mxSetField(obj, 0, "series_dates", ctx->arrDates);
				ctx->arrFields = mxCreateCellMatrix(ctx->n_fields, 1);
				mxAddField(obj, "series_fields");
				mxSetField(obj, 0, "series_fields", ctx->arrFields);
				if (g_log) fprintf(g_log, "Has fields: %d and points: %d\n", (int) ctx->n_fields, (int) ctx->n_points);
			}
		} else {
			dec->arr[level] = mxCreateCellMatrix(1, len);
			mxDestroyArray(points);
			mxSetField(dec->obj[1], 0, key, dec->arr[level]);
		}
		return len;
	}
	return 0;
}

static void setItem(struct __SJTSObjectDecoder *dec, int idx)
{
	SJTSContext *ctx = (SJTSContext *)dec->prv;

	if (ctx->output_type) {
		mxArray *fields = mxGetField(dec->obj[1], 0, "fields");
		for (int j = 0; j < ctx->n_fields; j++) {
		    if (ctx->idx == 0) mxSetCell(ctx->arrFields, j, mxCreateString(mxGetFieldNameByNumber(fields, j)));
			mxSetCell(ctx->arrValues, ctx->n_fields * idx + j, mxDuplicateArray(mxGetFieldByNumber(fields, 0, j)));
		}
	    mxArray *id = mxGetField(dec->obj[1], 0, "series_id");
		mxSetCell(dec->arr[0], idx, mxDuplicateArray(id));
		mxDestroyArray(dec->obj[1]);
	} else {
		mxSetCell(dec->arr[0], idx, dec->obj[1]);
	}
	ctx->idx = idx + 1;
}

static mxArray *to_iso(JSINT64 date)
{
	char buf[64];
	milli_to_iso(buf, date);
	return mxCreateString(buf);
}

static void setPoint(struct __SJTSObjectDecoder *dec, int idx, JSINT64 date, double value)
{
	SJTSContext *ctx = (SJTSContext *)dec->prv;

	if (ctx->output_type) {
		if (idx < ctx->n_points) {
			if (ctx->idx == 0) {
				mxSetCell(ctx->arrDates, idx, to_iso(date));
			}
			mxGetPr(dec->arr[1])[ctx->idx * ctx->n_points + idx] = value;
		}
	} else {
		mxArray *pnt = mxCreateCellMatrix(1, 2);
		mxSetCell(pnt, 0, to_iso(date));
		mxSetCell(pnt, 1, mxCreateDoubleScalar(value));
		mxSetCell(dec->arr[1], idx, pnt);
	}
}

mxArray *
decodeSJTS(const char *json_str, size_t json_size, int output_type)
{
	JSUINT64 json_offs = 0;
	mxArray *obj;
	SJTSContext sjCtx;
	SJTSObjectDecoder decoder =
	{
		{
			Object_newString,
			Object_objectAddKey,
			Object_arrayAddItem,
			Object_newTrue,
			Object_newFalse,
			Object_newNull,
			Object_newObject,
			Object_newArray,
			Object_newInteger,
			Object_newLong,
			Object_newUnsignedLong,
			Object_newDouble,
			Object_releaseObject,
			mxMalloc,
			mxFree,
			mxRealloc,
			NULL,
			NULL,
			0,
			&sjCtx,
		},
		getCount,
		setItem,
		setPoint
	};

	decoder.ujson.preciseFloat = 0;
	decoder.prv = &sjCtx;

	decoder.ujson.errorStr = NULL;
	decoder.ujson.errorOffset = NULL;
	sjCtx.output_type = output_type;
	obj = SJTS_DecodeObject(&decoder, json_str, json_size);
	if (g_log) fprintf(g_log, "Finish decoding...\n");
	return obj;
}
