classdef Shooju < handle
    % ver = Shooju.GetVersion();
    % Shooju.Connect(server, user, api_key);
    % res = Shooju.LoadSeries(params);
    
    % Copyright 2015 Shooju, LLC

    properties (Constant)
	VERSION = '3.2.0'
    end
    
    properties (Access = private)
        server % server URL
        user % user name
        api_key % password
        date_format % date format (not user-controlled)
        partial_ping_url %used for testing the connection
        partial_search_url %URL to append to the server URL when getting data from server
        search_url = 0 %full search URL
        partial_scroll_url
        scroll_url
        query_size %static and not user-controlled
        enable_debug_log
        enable_debug_dump
        job_id
        job_batch
        job_batch_size
    end
    
    methods (Access = public, Static)
        function res = GetVersion()
		res = strjoin({Shooju.VERSION, '(mex ', mexshooju(), ')'}, '');
	end

        function Connect(server, user, api_key)
            % Connects to Shooju server using user and api key.  All
            % parameters are required.
            %
            % Parameters:
            % server -- the full Shooju server address, starting with
            %      https://, or the short server name
            % user -- your Shooju username
            % api_key -- your API Key. To find out the API Key, go to
            %      Action=>Settings in the Shooju web interface.
            
            
            if nargin < 3
                error('Connect: must provide server, user and api_key params')
            end
            this = Shooju.instance();
            if regexp(server, '^http(s)?://','ignorecase')
                this.server = server;
            else
                this.server = strcat('https://', server, '.shooju.com');
            end
            this.user = user;
            this.api_key = api_key;
            data = mexshooju(this.server, this.user, this.api_key);
            if ~data.success && ~isempty(data.description)
                error(strcat('Connect: ', data.description))
            end

            this.search_url = strcat(this.server, this.partial_search_url);
            this.scroll_url = strcat(this.server, this.partial_scroll_url);
        end

        function EnableDebugLog(val)
            % Enable debug logs for mexshooju function requests
            %

            this = Shooju.instance();
            this.enable_debug_log = val;
        end

        function EnableDebugDump(val)
            % Enable debug dumps for mexshooju function requests to files
            %

            this = Shooju.instance();
            this.enable_debug_dump = val;
        end

        function RegisterJob(description, batch_size)
            % Register a new job. All parameters are optional.
            %
            % Parameters:
            % description -- a description for a new job
            %       string (default: 'no description')
            % batch_size -- number of series before auto submit
            %       number (default: 0)
            %

            if nargin < 1
                description = 'no description';
            end
            if nargin < 2 || batch_size < 0
                batch_size = 0;
            end
            this = Shooju.instance();
            this.job_batch_size = batch_size;
            this.job_batch = [];
            if this.job_id >= 0
                error(['There is already an active job (#' ...
                    num2str(this.job_id) ...
                    '). Call Shooju.Finish() to finish it, ' ...
                    'and then Shooju.RegisterJob() again to start a new job.'])
            end
            data = this.mex_method('register_job', description);
            this.job_id = data.job_id;
        end

        function res = JobId()
            % Returns the active job Id
            %

            this = Shooju.instance();
            res = this.job_id;
        end

        function FinishJob()
            % Finish an active job
            %

            this = Shooju.instance();
            this.SubmitJob();
            this.mex_method('finish_job', this.job_id);
            this.job_id = -1;
        end

        function SubmitJob()
            % Send all writes to server
            %

            this = Shooju.instance();
            if this.job_id < 0
                error('There is no active job. Call Shooju.RegisterJob() to start it.')
            end
            if isempty(this.job_batch)
                return
            end
            for i = 1:length(this.job_batch)
                if ~isempty(this.job_batch(i).fields)
                    [this.job_batch(i).fields, idx] = unique(this.job_batch(i).fields, 'last');
                    this.job_batch(i).fielddata = this.job_batch(i).fielddata(idx);
                end
                if ~isempty(this.job_batch(i).dates)
                    [this.job_batch(i).dates, idx] = unique(this.job_batch(i).dates, 'last');
                    this.job_batch(i).pointdata = this.job_batch(i).pointdata(idx);
                end
            end
            this.mex_method('write', {this.job_id; this.job_batch});
            this.job_batch = [];
        end

        function ResetBatch()
            this = Shooju.instance();
            this.job_batch = [];
        end

        function res = RawPostSeries(api_path, url_params, matrix_2N)
            % Make a raw post for series
            %
            % Parameters:
            % api_path -- url for a raw post
            %       string
            % url_params -- additional parameters
            %       struct
            % matrix_2N -- point data
            %       cells

            this = Shooju.instance();
            params = {};
            d = matrix_2N(:,1);
            params.point_dates = strcat(cellstr(datestr(d,'yyyy-mm-dd')), {'T'}, cellstr(datestr(d,'HH:MM:SS')));
            params.point_data = num2cell(matrix_2N(:,2));
            r = this.mex_method('raw_post', {api_path; url_params; params});
            dates = cellfun(@this.isoToDatenum, r.series_dates);
            values = r.series_points;
            res = cat(2, dates, values);
        end

        function Write(series_query, fields, fielddata, dates, pointdata, remove_others)
            % Write fields/points to server for series query
            % First parameter is required, others are optional
            %
            % Parameters:
            % series_query -- Shooju series query for writing fields/points
            %       string
            % fields -- field names
            %       string or cell of strings
            % fielddata -- field values
            %       string/number or cell of string/number
            % dates -- point dates
            %       string or cell of strings in the ISO format: 'YYYY-MM-DD'
            % pointdata -- point values
            %       number or cell of numbers
            % remove_others -- additional option for fields/points processing
            %       string: 'all', 'fields', 'points' (default: empty)
            %

            this = Shooju.instance();
            if this.job_id < 0
                error('There is no active job. Call Shooju.RegisterJob() to start it.')
            end
            if isempty(this.job_batch)
                idx = [];
            else
                idx = find(strcmp({this.job_batch.series_query}, series_query)==1);
            end
            
            if isa(dates, 'double')
                % avoid using T because it causing errors in early MatLab versions
                dates = strcat(cellstr(datestr(dates,'yyyy-mm-dd')), {'T'}, cellstr(datestr(dates,'HH:MM:SS')));           
            end
            
            if ~iscell(fields); fields = {fields};fielddata = {fielddata};end
            if isrow(fields); fields = fields'; end
            if isrow(fielddata); fielddata = fielddata'; end
            if size(fields,1) ~= size(fielddata,1)
                error('number of field data doesn''t match with fields')
            end
            for i = 1:length(fields)
                if ~ischar(fields{i})
                    error('only string names of fields are supported')
                end
                if iscell(fielddata{i})
                    for j = 1:length(fielddata{i})
                        if ~ischar(fielddata{i}{j}) && ~isnumeric(fielddata{i}{j})
                            error('only strings and numeric fields are supported as elements of array')
                        end
                    end
                elseif ~ischar(fielddata{i}) && ~isnumeric(fielddata{i})
                    error('only array, strings and numeric fields are supported')
                end
            end
            if isempty(idx)
                if length(this.job_batch) > this.job_batch_size
                    this.SubmitJob()
                end
                idx = length(this.job_batch) + 1;
                this.job_batch(idx).series_query = series_query;
                this.job_batch(idx).fields = {};
                this.job_batch(idx).fielddata = {};
                this.job_batch(idx).dates = {};
                this.job_batch(idx).pointdata = {};
                this.job_batch(idx).remove_others = 'none';
            end
            this.job_batch(idx).fields = cat(1, this.job_batch(idx).fields, fields);
            this.job_batch(idx).fielddata = cat(1, this.job_batch(idx).fielddata, fielddata);
            if nargin > 3
                if ~iscell(dates); dates = cellstr(dates); end
                if ~iscell(pointdata); pointdata = num2cell(pointdata); end
                if isrow(dates); dates = dates'; end
                if isrow(pointdata); pointdata = pointdata'; end
                if size(dates,1) ~= size(pointdata,1)
                    error('number of point data doesn''t match with dates')
                end
                for i = 1:length(dates)
                    if ~ischar(dates{i})
                        error('only string dates of points are supported')
                    end
                    if ~isnumeric(pointdata{i})
                        error('only numeric points are supported')
                    end
                end
                this.job_batch(idx).dates = cat(1, this.job_batch(idx).dates, dates);
                this.job_batch(idx).pointdata = cat(1, this.job_batch(idx).pointdata, pointdata);
                if nargin > 5
                    opts = {'all' 'fields' 'points'};
                    if ~ismember(opts, remove_others)
                        error(strcat({'remove_otherss must be one of '}, ...
                            string(join(opts, ', ')), ' or null/empty'));
                    end
                    this.job_batch(idx).remove_others = remove_others;
                end
            end
            if length(this.job_batch) > this.job_batch_size
                this.SubmitJob()
            end
        end

        function res = LoadSeries(params)
            % Loads series into struct.
            % res.series = array of series
            % res.fields = array of fields
            % res.fielddata = array of field values
            %           rows are fields
            %           columns are series
            % res.points = array of fields
            % res.pointdata = array of point values
            %           rows are dates
            %           columns are series
            
            % Parameters:
            % query -- Shooju query
            % column_field (default: series_id) -- the field to use in the
            %      column headers (might be adjusted to meet MATLAB valid
            %      column criteria)
            % date_from (default: MIN) -- must be either MIN or a date/time
            %      string in the ISO format: 'YYYY-MM-DD'
            % date_to (default: MAX) -- must be either MAX or a date/time
            %      string in the ISO format: 'YYYY-MM-DD'
            % max_rows (default: 10) -- max rows to return for each series

            if nargin ~= 1 || ~isstruct(params)
                error('LoadSeries: must provide params struct')
            end
            api_params = params;
            if ~isfield(params, 'query')
                error('LoadSeries: must provide at least query param')
            end
            blacklist = {'date_format', 'include_job', 'include_timestamp', 'location_type'};
            black_params = intersect(fieldnames(params), blacklist);
            if ~isempty(black_params)
                error('LoadSeries: params contains forbidden params: %s', black_params{:});
            end

            if ~isfield(params, 'df')
                api_params.('df') = 'MIN';
            else
                if ~ischar(params.df)
                    error('LoadSeries: df should be a date/time string in ISO format or MIN')
                end
            end
            
            if ~isfield(params, 'dt')
                api_params.('dt') = 'MAX';
            else
                if ~ischar(params.dt)
                    error('LoadSeries: dt should be a date/time string in ISO format or MAX')
                end
            end
            
            if ~isfield(params, 'max_points')
                api_params.('max_points') = '10';
            else
                if isnumeric(params.max_points)
                    api_params.max_points = num2str(params.max_points);
                end
            end
            
            convert_to_datenum = false;
            if isfield(params, 'dates_type')
                
               if strcmp(params.dates_type, 'datenum')
                  convert_to_datenum = true;
               elseif ~strcmp(params.dates_type, 'iso')
                   error('dates_type must be either datenum or iso')
               end
            end
            
            if ~isfield(params, 'fields')
                api_params.('fields') = 'series_id';
            else
                if iscell(params.fields)
                    if isempty(params.fields)
                        fields = '';
                    else
                        fields = params.fields{1};
                        for n = 2:length(params.fields)
                            fields = strcat(fields, ',', params.fields{n});
                        end
                    end
                else
                    fields = params.fields;
                end
                if strcmp(fields, 'sid') %backwards compatibility
                    fields = 'series_id';
                end
                api_params.fields = fields;
            end
            api_params.('scroll') = 'true';
            
            this = Shooju.instance();
            api_names = fieldnames(api_params);
            api_array = cell(1, length(api_names) * 2);
            for i = 1:length(api_names)
                api_array{i * 2 - 1} = api_names{i};
                api_array{i * 2} = api_params.(api_names{i});
            end
            res = this.get_table(api_array);
            
            if convert_to_datenum
               res.dates = cellfun(@this.isoToDatenum, res.dates);             
            end
        end
    end

    methods (Access = private)
        function this = Shooju()
            if exist('mexshooju', 'file') ~= 3
                error('Mexshooju not found!')
            end
            this.server = '';
            this.user = '';
            this.api_key = '';
            this.date_format = 'iso';
            this.partial_ping_url = '/api/1/ping';
            this.partial_search_url = '/api/1/series';
            this.partial_scroll_url = '/api/1/series';
            this.query_size = 250;
            this.enable_debug_log = false;
            this.enable_debug_dump = false;
            this.job_id = -1;
        end

        function res = get_results(~, data)
            res = struct('series', [], 'fields', [], 'fielddata', [], 'dates', [], 'pointdata', []);
            if isfield(data, 'series_points') && isfield(data, 'series_values')
                res.series = data.series;
                res.fields = data.series_fields;
                res.fielddata = data.series_values;
                res.dates = data.series_dates;
                res.pointdata = data.series_points;
            else
                res = Shooju.reply2res(data);
            end
        end

        function res = get_table(this, params)
            res = struct('series', [], 'fields', [], 'fielddata', [], 'dates', [], 'pointdata', []);
            if ~ischar(this.search_url)
                error('Must call Shooju.Connect first')
            end
            data = this.send_request(params);
            if 0 == data.total
                return
            end
            res = this.get_results(data);
            if length(res.series) < data.total
                while 1
                    data = this.send_request(data.scroll_id);
                    if ~isempty(data.series)
                        tmp_res = this.get_results(data);
                        res.series = horzcat(res.series, tmp_res.series);
                        res.fielddata = horzcat(res.fielddata, tmp_res.fielddata);
                        res.pointdata = horzcat(res.pointdata, tmp_res.pointdata);
                    else
                        break
                    end
                end
            end
        end

        function data = mex_method(this, method, params)
            if ~ischar(this.search_url)
                error('Must call Shooju.Connect first')
            end
            flags = 1;
            if this.enable_debug_log
                flags = flags + 2;
            end
            if this.enable_debug_dump
                flags = flags + 4;
            end
            params_cell = cat(1, {'method'; method}, params);
            data = mexshooju(this.server, this.user, this.api_key, ...
                params_cell, flags);
            if isfield(data, 'success') && data.success ~= 1
                error(data.description)
            elseif isfield(data, 'responses')
                for i=1:length(data.responses)
                    if isfield(data.responses{i}, 'error')
                        error(data.responses{i}.description)
                    end
                end
            end
        end

        function data = send_request(this, params)
            if ischar(params)
                api_params = {'scroll_id', params};
            else
                api_params = params;
            end
            flags = 1;
            if this.enable_debug_log
                flags = flags + 2;
            end
            if this.enable_debug_dump
                flags = flags + 4;
            end
            data = mexshooju(this.server, this.user, this.api_key, api_params, flags);
            if 0 == data.success
                error(strcat('API Error: ', data.description))
            end
        end
    end
    methods(Access = private, Static)
        function obj = instance()
            persistent uniqueInstance
            if isempty(uniqueInstance)
                obj = Shooju();
                uniqueInstance = obj;
            else
                obj = uniqueInstance;
            end
        end

        function res = reply2res(data)
            res = struct('series', [], 'fields', [], 'fielddata', [], 'dates', [], 'pointdata', []);
            if 1 ~= data.success
                error('reply2res: invalid data')
            end
            if isempty(data.series)
                %end of the scroll request or empty reply
                return
            end
            if isempty(data.series{1}.fields)
                return
            end
            cols = size(data.series, 2);
            res.series = cell(1, cols);
            for c = 1:cols
                res.series{1,c} = data.series{c}.series_id;
            end
            if isfield(data.series{1}, 'points')
                rows = size(data.series{1}.points, 2);
            else
                rows = 0;
            end
            pointdata = nan(rows, cols, 'double');
            res.dates = cell(rows, 1);
            for r = 1:rows
                res.dates{r} = data.series{1}.points{1,r}{1};
            end
            for r = 1:rows
                for c = 1:cols
                    val = data.series{c}.points{r}{2};
                    if ~isempty(val)
                        pointdata(r,c) = val;
                    end
                end
            end
            res.fields = fieldnames(data.series{1}.fields);
            rows = length(res.fields);
            res.fielddata = cell(rows, cols);
            for c = 1:cols
                for r = 1:rows
                    val = data.series{c}.fields.(res.fields{r});
                    if isempty(val)
                        res.fielddata{r,c} = {NaN};
                    else
                        res.fielddata{r,c} = val;
                    end
                end
            end
            res.pointdata = pointdata;
        end

        function final = make_unique(orig)
            valid_orig = Shooju.validate_column_names(orig);
            final = valid_orig;
            for n = 1:length(valid_orig)
                idx = find(ismember(valid_orig, valid_orig(n)));
                if 1 < length(idx)
                    for k = 1:length(idx)
                        elem = valid_orig(idx(k));
                        final(idx(k)) = {strcat(elem{1}, '_', num2str(k))};
                    end
                end
            end
        end

        function col_names = validate_column_names(orig_col_names)
            col_names = orig_col_names;
            for n = 1:length(orig_col_names)
                if ~isvarname(orig_col_names(n))
                    %col_names(n) = matlab.lang.makeValidName(orig_col_names(n));
                end
            end
        end

        function col_names = intersect_column_names(ref_col_names, tmp_col_names)
            inter = intersect(ref_col_names, tmp_col_names);
            col_names = tmp_col_names;
            for n = 1:length(inter)
                idx = find(ismember(tmp_col_names, inter(n)));
                if 0 < length(idx)
                    for k = 1:length(idx)
                        elem = tmp_col_names(idx(k));
                        col_names(idx(k)) = {strcat(elem{1}, '_', num2str(k))};
                    end
                end
            end
        end

        function out = cell2str(in)
            out = in;
            if ~iscellstr(in)
                for n = 1:length(in)
                    if ~ischar(in{n})
                        out{n} = num2str(in{n}, '%4.4e');
                    end
                end
            end
        end

        function y = isoToDatenum(dt)
            isoDates = strcat({dt(1:10)}, {' '}, {dt(12:19)});
            y = datenum(isoDates, 'yyyy-mm-dd HH:MM:SS');
        end
    end
end
