cd openssl
nmake
cd ../libevent
set OPENSSL_DIR=../openssl
nmake -f Makefile.nmake static_libs
cd ../sjts
nmake -f Makefile.nmake
cd ../c-client
nmake -f Makefile.nmake
cd ..
call mex  ../mexshooju.c c-client/libshooju.lib libevent/libevent.lib wsock32.lib Ws2_32.lib openssl/libssl.lib openssl/libcrypto.lib libevent/libevent_openssl.lib crypt32.lib sjts/libsjts.lib
copy mexshooju.mexw* ..\