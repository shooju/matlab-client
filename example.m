%% Shooju MATLAB Client Sample Usage
% This example file demonstrates basic usage.

%% Connect to Shooju Server
% Connect and authenticate with Shooju server.  Use the full Shooju server 
% address or the short account name.  To find your API Key, go to 
% Action=>Settings in the Shooju web interface.
Shooju.Connect(SERVER, USER, API_KEY);


%% Load Points
% Load points into a table where columns are series and rows are
% dates.  Use a Shooju query, dt, df and max_points. See Function Hints for more info.

tic
Shooju.EnableDebugLog(true);
r1_mex = Shooju.LoadSeries(struct('query', QUERY, 'df', '2015-02-01T11:30', 'dt', DATE_TO, 'max_points', MAX_ROWS, 'type', 'test'));
Shooju.EnableDebugLog(false);
toc

%% Load Fields
% Load field values into a table where columns are fields and rows are
% series.  Use a Shooju query and Shooju fields.  See Function Hints for 
% more info.
tic
r2_mex = Shooju.LoadSeries(struct('query', QUERY, 'fields', {{FIELD1, FIELD2, FIELD3, FIELD4}}));
toc
