%% Connect
Shooju.Connect(SERVER, USER, API_KEY);
Shooju.EnableDebugLog(true);

%% RawPost
Shooju.EnableDebugDump(true);
d1 = datenum(2021,2,1,10,30,15);
d2 = datenum(2021,2,2,11,40,20);
d3 = datenum(2021,2,3,12,45,25);
mat = [[d1,1];[d2,2];[d3,3]];
url = '/processors/f/call/bucketize_external';
url_params = struct('iso', 'PJM', 'price_band', '2x16');
res = Shooju.RawPostSeries(url, url_params, mat);
Shooju.EnableDebugDump(false);
