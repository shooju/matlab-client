addpath('..')
try
    Shooju.Connect('WRONG_ADDRESS', USER, API_KEY);
    error('Should be a connection error!')
catch err
    assert(strcmp(err.identifier, 'MATLAB:urlread:UnknownHost'), sprintf('Wrong connection error: %s', err.identifier))
end

try
    Shooju.Connect('test1', USER, 'INVALID_API_KEY');
    error('Should be a connection error!')
catch err
    assert(strcmp(err.message, 'Connect:Invalid ''user'' and/or ''api_secret''.'), sprintf('Wrong connection error: %s', err.message))
end
try
    Shooju.Connect('test1', USER, API_KEY);
catch err
    error('Should connect without errors, but there''s one: %s', err.message)
end
try
    Shooju.Connect('https://test1.shooju.com', USER, API_KEY);
catch err
    error('Should connect without errors, but there''s one: %s', err.message)
end
