addpath('..')
Shooju.Connect(SERVER, USER, API_KEY)
assert(Shooju.JobId == -1, 'JobID should be -1 before RegisterJob')
% Batch size = 0
Shooju.RegisterJob('test')
assert(Shooju.JobId ~= -1, 'JobID should not be -1 after RegisterJob')
try
    Shooju.RegisterJob('test')
    error('Should raise error')
catch err
    assert(strfind(err.message, 'There is already an active job')==1, sprintf('Wrong connection error: %s',err.message))
end
curTime = datestr(clock,31);
sid = sprintf('sid=users\\%s\\matlab-tests', USER);
Shooju.Write(sid, {}, {}, {curTime}, {1})
res = Shooju.LoadSeries(struct('query', sid, 'df', curTime, 'dt', curTime, 'max_points', 10, 'type', 'test'));
assert(length(res.pointdata)==1 && res.pointdata==1, 'Write should send to server immediately')
Shooju.FinishJob()
% Batch size = 1
Shooju.RegisterJob('test', 1)
Shooju.Write(sid, {}, {}, {curTime}, {2})
res = Shooju.LoadSeries(struct('query', sid, 'df', curTime, 'dt', curTime, 'max_points', 10, 'type', 'test'));
assert(length(res.pointdata)==1 && res.pointdata==1, 'Write should not send to server immediately')
Shooju.FinishJob()
res = Shooju.LoadSeries(struct('query', sid, 'df', curTime, 'dt', curTime, 'max_points', 10, 'type', 'test'));
assert(length(res.pointdata)==1 && res.pointdata==2, 'Write should send to server before FinishJob')
