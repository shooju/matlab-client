addpath('..')
Shooju.Connect(SERVER, USER, API_KEY);
sid = sprintf('sid=users\\%s\\matlab-tests', USER);
aMonthAgo = datestr(datetime('now') - calmonths(1) - caldays(1),31);
curTime = datestr(datetime('now'),31);
nextDay = datestr(datetime('now') + caldays(1),31);
max_fields = 50;
fields = strcat('field',strtrim(cellstr(num2str([0:max_fields]'))'));
values = strcat('value',strtrim(cellstr(num2str([0:max_fields]'))'));
Shooju.RegisterJob('test')
Shooju.Write(sid, fields, values, {curTime aMonthAgo nextDay}, [1 2 3], 'all')
Shooju.FinishJob()
res = Shooju.LoadSeries(struct('query', sid, 'max_points', 10, 'fields', strjoin(fields,',')));
assert(length(res.fields) == length(fields), 'Number of fields missmatch')
assert(length(res.dates) == 3, 'Number of points missmatch')
res = Shooju.LoadSeries(struct('query', sid, 'max_points', 10, 'dt', '-1M','dates_type', 'datenum'));
assert(length(res.dates) == 1, 'Special values for dt don''t work')
assert(res.dates(1) == datenum(aMonthAgo(1:10)), 'dates_type doesn''t work')
res = Shooju.LoadSeries(struct('query', sid, 'max_points', 10, 'df', curTime, 'dt', 'MAX'));
assert(length(res.dates) == 2, 'df doesn''t work')
res = Shooju.LoadSeries(struct('query', sid, 'max_points', 2));
assert(length(res.dates) == 2, 'max_points is not respected')
res = Shooju.LoadSeries(struct('query', strcat(sid, '@S:M'), 'max_points', 10));
assert(res.pointdata(1) == 2 && (res.pointdata(2) == 4 || res.pointdata(2) == 1), 'Aggregation doesn''t work')