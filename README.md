# shooju

shooju is the official MATLAB client library for Shooju with the following features:

* Authentication via username and api key
* Getting series and fields


## Connect to api
Connect with `Shooju.Connect('https://my-account.shooju.com', 'my-user', 'my-api-key')`

## Load one or more series by using a query:

```
res = Shooju.LoadSeries(struct('query','my-query','max_points','-1','fields','description,unit'))
```
A few notes on using this function:

The only argument is a struct of parameter/values that get passed to the /series API.  For a full list of available parameters, refer to the GET /series documentation at API Reference
The result of this call is a struct with 5 fields:


- **series** - Horizontally-oriented cell array with all returned Series IDs.  This is often not used in favor of more descriptive fields.
- **fields** - Vertically-oriented cell array with all returned fields.
- **fielddata**	- Cell array that contains the values for each of the fields / series.  The rows are fields (as defined by the fields key) and the columns are series (as defined by the series key).
- **dates** - Vertically-oriented cell array with all returned dates.
- **pointdata**	- Cell array that contains the double values for each of the dates / series.  The rows are dates (as defined by the dates key) and the columns are series (as defined by the series key).


By default `LoadSeries()` returns dates as an array of ISO strings. To change `dates` types use `dates_type` parameter. Possible values: `iso` or `datenum`. Example:

```
res = Shooju.LoadSeries(struct('query','my-query','max_points','-1','fields','description,unit', 'dates_type', 'datenum'))
```

## Write data to Shooju

Writing data is 3 step process: create a job, write data, finish job:

```
Shooju.RegisterJob('job description', batch_size=100)
Shooju.Write('sid=example\1', {'description', 'symbol'}, {'Currency rate USD/EUR', 'EURUSD'}, {'2010-01-01T00:00:00.000'. '2010-01-02T00:00:00.000', '2010-01-03T00:00:00.000'}, {0.1, 0.66, nan})
Shooju.Write('sid=example\2', {'description', 'symbol'}, {'Currency rate USD/GBP', 'EURGBP'})
Shooju.Write('sid=example\3', {}, {}, {'2010-01-01T00:00:00.000', '2010-01-02T00:00:00.000', '2010-01-03T00:00:00.000'}, {10, 20.10, 0.5}, 'points')
Shooju.Write('sid=example\4', {'description', 'symbol'}, {'Currency rate USD/EUR', 'EURUSD'}, [datenum(2001, 1, 1), datenum(2001, 1, 3), datenum(2001, 1, 4)], {0.1, 0.66, 10})
Shooju.FinishJob()
```

By default, **Write()** call send data to Shooju immediately.  When making many **Write()** calls, it is recommended to queue **Write()** calls and submit them in batches.  This is done by specifying a **batch_size** when registering the job.

`Shooju.Write()` accepts the following parameters:
- **series_query** (required) - Series query that identifies a series
- **fields** - Vertically-oriented cell array with written field names
- **fielddata** - Cell array that contains the values for each of the fields. Must be the same length as **fields** array.
- **dates** - Vertically-oriented cell array with points dates represented in ISO format. (e.g 2001-01-10, 2010-01-02T00:00:00.000)
- **pointdata** - Cell array that contains the double values for each of the dates. 
- **remove_others** - Optional flag indicating that series data that aleady stored in Shooju will be removed and replaced by written data. 
                      Accepts either `points` (only existing points will be replaced), `fields` (to replace fields) and `all` (for fields and points).

To remove specific point or field write `nan` as a value.
`Write()` accepts dates either as iso strings or datenums.


# Building

1. Check that `mex` is available from the command line and add if necessary full path for it to PATH (for example for 64-bit: `<MATLAB PATH>\bin\win64\`)
2. Make sure that MATLAB supports installed Visual Studio version by running `mex -setup` and following instructions to enable compiler support
3. Extract the latest openssl source archive from https://www.openssl.org/source/ to `build/openssl`
4. Install Perl (for building openssl library) from http://strawberryperl.com/
5. Configure and build openssl library.  Use `perl Configure VC-WIN32 no-asm no-shared` for 32-bit and `perl Configure VC-WIN64A no-asm no-shared` for 64-bit (must match MATLAB)
6. Clone libevent library https://github.com/a212/libevent.git to `build/libevent`
7. Clone Shooju sjts library https://bitbucket.org/shooju/sjts.git/branch/matlab to `build/sjts`
8. Clone Shooju c-client library https://bitbucket.org/shooju/c-client.git to `build/c-client`
9. Open VS command prompt (x86 or x64) and run `build.bat` from the `build` directory
